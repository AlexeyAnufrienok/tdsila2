<?php
// Headers
$_['heading_title']    = 'Product Barcode';

// Text
$_['text_edit']        = 'Редактировать модуль Product Barcode';
$_['text_extension']   = 'Расширения';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас не хватает прав на изменение модуля!';
